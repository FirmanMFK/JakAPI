package id.firman.jakartaapi.network;

import id.firman.jakartaapi.response.MuseumResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by firman on 01/12/17.
 */

public interface ApiService {

    @Headers({"Authorization: " + "v7kdImGQlCfjD+sg7DGlxFtNBJgB8+oXotwo3yqp2LfWV3sw3/5OrxQ1wXLPqLsF"})
    @GET("museum")
    Call<MuseumResponse> getMuseumData();
}
