package id.firman.jakartaapi.model;

import lombok.Data;

/**
 * Created by firman on 02/12/17.
 */
@Data
public class GambarList {
    public GambarList() {
    }

    private int gambar;

    public GambarList(int gambar) {
        this.gambar = gambar;
    }
}

