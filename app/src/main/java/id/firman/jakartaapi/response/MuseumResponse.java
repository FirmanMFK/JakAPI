package id.firman.jakartaapi.response;

import java.util.List;

import lombok.Data;

/**
 * Created by firman on 01/12/17.
 */
@Data
public class MuseumResponse {
    private List<ResponseMuseumData> data;

    @Data
    public class ResponseMuseumData {
        private int id;
        private String nama_museum;
        private String alamat;
        private String deskripsi;
        private String link;
        private double latitude;
        private double longitude;




    }
}
