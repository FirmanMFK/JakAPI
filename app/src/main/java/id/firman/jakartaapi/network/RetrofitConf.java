package id.firman.jakartaapi.network;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by firman on 01/12/17.
 */

public class RetrofitConf {

    // Two Static Class
    //Initialization Config
    //The Retrofit class generates an implementation of the api jakarta
    public static Retrofit setInitRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://api.jakarta.go.id/v1/")
               .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiService getInitRetrofit() {
        return setInitRetrofit().create(ApiService.class);
    }
}
